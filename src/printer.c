#include "printer.h"
#include <stdio.h>

void print_passed_test(int test_number){
    fprintf(stdout, "Test #%d Passed\n", test_number);
}

void print_failed_test(int test_number) {
    fprintf(stderr, "Test #%d Failed\n", test_number);
}

void print_started_test(int test_number){
    fprintf(stdout, "Test #%d Started\n", test_number);
}
