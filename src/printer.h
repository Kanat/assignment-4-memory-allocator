#ifndef PRINTER_H
#define PRINTER_H
#include <stdio.h>

void print_passed_test(int test_number);
void print_failed_test(int test_number);
void print_started_test(int test_number);

#endif
