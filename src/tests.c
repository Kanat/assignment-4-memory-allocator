#include "printer.h"
#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include <stdio.h>

#define HEAP_SIZE 8192


static inline void destroy_heap(void* heap, size_t size) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = size}
            ).bytes);
}

static inline struct block_header* find_header(void* contents) {
    return (struct block_header*) ((uint8_t*) (contents) - offsetof(struct block_header, contents));
}

void first_test(void) {
    print_started_test(1);
    void* heap = heap_init(HEAP_SIZE); //8192
    debug_heap(stdout, heap);

    void* data = _malloc(2000); 
    debug_heap(stdout, heap);

    if (!heap || !data) {
        print_failed_test(1);
        return;
    }

    _free(data);
    debug_heap(stdout, heap);
    
    destroy_heap(heap, HEAP_SIZE);
    print_passed_test(1);
}

void second_test(void) {
    print_started_test(2);
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void* data1 = _malloc(128);
    void* data2 = _malloc(256);
    debug_heap(stdout, heap);
    if (!heap || !data1 || !data2) {
        print_failed_test(2);
        return;
    }

    _free(data1); //free one data block
    debug_heap(stdout, heap);
    destroy_heap(heap, HEAP_SIZE);
    print_passed_test(2);
}

void third_test(void) {
    print_started_test(3);
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void* data1 = _malloc(128);
    void* data2 = _malloc(64);
    void* data3 = _malloc(256); //dont free this data block
    debug_heap(stdout, heap);

    if (!heap || !data1 || !data2 || !data3) {
        print_failed_test(3);
        return;
    }

    _free(data1);
    _free(data2);
    debug_heap(stdout, heap);
    destroy_heap(heap, HEAP_SIZE);
    print_passed_test(3);
}

void fourth_test(void) {
    print_started_test(4);
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void* data1 = _malloc(2 * HEAP_SIZE); //allocating more memory than heap size
    void* data2 = _malloc(2 * HEAP_SIZE); 
    debug_heap(stdout, heap);

    struct block_header* first_header = find_header(data1);
    if (!heap || !data1 || !data2 || first_header->next != find_header(data2)) {
      print_failed_test(4);
      return;
    }   

    _free(data1);
    _free(data2);

    debug_heap(stdout, heap);   
    destroy_heap(heap, HEAP_SIZE);

    print_passed_test(4);
}

void fifth_test(void) {
    print_started_test(5);
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);   

    void* data1 = _malloc(2 * HEAP_SIZE);
    struct block_header* header = find_header(data1);

    if (!heap || !data1) {
      print_failed_test(5);
      return;
    }   

    (void) mmap(header->contents + header->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);    
    
    // if (!temp_mmap) {
    //     print_failed_test(5);
    //     return;
    // }

    void* data2 = _malloc(2 * HEAP_SIZE);
    debug_heap(stdout, heap);

    if (!data2 || (find_header(data2) == (void*)(header->contents + header->capacity.bytes))) {
      print_failed_test(5);
      return;
    }   

    _free(data1);
    _free(data2);

    debug_heap(stdout, heap);   
    destroy_heap(heap, 2 * HEAP_SIZE);
    print_passed_test(5);
}

void start_all_tests(void){
    first_test();
    second_test();
    third_test();
    fourth_test();
    fifth_test();
}

