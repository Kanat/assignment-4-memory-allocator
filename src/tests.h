#ifndef MEM_ALLOC_TEST_H
#define MEM_ALLOC_TEST_H

void first_test(void);

void second_test(void);

void third_test(void);

void fourth_test(void);

void fifth_test(void);

void start_all_tests(void);

#endif